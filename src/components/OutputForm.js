import React, { Component } from 'react';
import 'semantic-ui-css/semantic.min.css';
import './OutputForm.css';
import { FaCheck, FaTimes } from "react-icons/fa";

export default class OutputForm extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data: this.props.jsonObject,
            temp: false
        };
    }

    componentDidUpdate(prevProps) {
        // Typical usage (don't forget to compare props):
        if (this.props.jsonObject !== prevProps.jsonObject) {
            this.setState({ data: this.props.jsonObject });
        }
    }

    render() {
        // const log = this.state.data;
        // console.log(log);
        return (
            <>
                <div className="field" id="container">
                    <div id="titleText">
                        <h1>Choose a plan</h1>
                    </div>

                    <div className="ui column grid" id="colunmGrid">
                        <div className="column">
                            <div className="row" id="differencesNameBlockTop">
                                <p className="ui black label" id="planName">Service Plan Title</p>
                            </div>
                            <div className="row" id="differencesNameBlock">
                                <span id="differencesName">Basic Check</span>
                                <hr id="invisibleLine" />
                            </div>
                            <div className="row" id="differencesNameBlock">
                                <span id="differencesName">Diabetes Test</span>
                                <hr id="invisibleLine" />
                            </div>
                            <div className="row" id="differencesNameBlock">
                                <span id="differencesName">Blood Test</span>
                                <hr id="invisibleLine" />
                            </div>
                            <div className="row" id="differencesNameBlock">
                                <span id="differencesName">Urine Test</span>
                                <hr id="invisibleLine" />
                            </div>
                            <div className="row" id="differencesNameBlock">
                                <span id="differencesName">Heart Test</span>
                                <hr id="invisibleLine" />
                            </div>
                            <div className="row">
                                <span></span>
                            </div>
                        </div>

                        {this.state.data && this.state.data.map(post => {
                            return (
                                <>
                                    <div className="column">
                                        <div className="row">
                                            <p className="ui black label" id="planName">{post.planName}</p>
                                        </div>
                                        <div className="row">
                                            {post.basicBodyCheck
                                                ? <FaCheck id="tickIcon" />
                                                : <FaTimes id="crossIcon" />}
                                            <hr />
                                        </div>
                                        <div className="row">
                                            {post.diabetesTest
                                                ? <FaCheck id="tickIcon" />
                                                : <FaTimes id="crossIcon" />}
                                            <hr />
                                        </div>
                                        <div className="row">
                                            {post.bloodTest
                                                ? <FaCheck id="tickIcon" />
                                                : <FaTimes id="crossIcon" />}
                                            <hr />
                                        </div>
                                        <div className="row">
                                            {post.urineTest
                                                ? <FaCheck id="tickIcon" />
                                                : <FaTimes id="crossIcon" />}
                                            <hr />
                                        </div>
                                        <div className="row">
                                            {post.heartTest
                                                ? <FaCheck id="tickIcon" />
                                                : <FaTimes id="crossIcon" />}
                                            <hr />
                                        </div>
                                        <div className="row">
                                            <input type="radio" id="priceRadio" name="price_group" />
                                            <span id="priceValue"><b>HK${post.price}</b></span>
                                            <span id="priceText"><b>/Month</b></span>
                                        </div>
                                    </div>
                                </>
                            )
                        })}
                    </div>
                </div>
            </>
        )
    }
}
