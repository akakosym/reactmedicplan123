import React, { Component } from 'react';
import 'semantic-ui-css/semantic.min.css';
import './InputArea.css';
import swal from 'sweetalert';
import OutputForm from './OutputForm.js';

export default class JsonTextArea extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: '',
            jsonData: '',
            buttonClicked: false
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.setState({ value: event.target.value });
    }

    handleSubmit(event) {
        event.preventDefault();
        const stringData = this.state.value;
        try {
            const data = JSON.parse(stringData);
            this.setState({ jsonData: data });
            this.setState({ buttonClicked: true });
        } catch (e) {
            swal({
                title: 'Invalid JSON format!',
                icon: 'warning'
            });
        }

    }

    render() {
        const buttonClicked = this.state.buttonClicked;
        return (
            <div>
                <form className="ui form" onSubmit={this.handleSubmit}>
                    <div className="field">
                        <h1>Input Json below:</h1>
                        <textarea
                            rows="20"
                            id="textArea"
                            value={this.state.value}
                            onChange={this.handleChange}
                        />
                    </div>
                    <button className="ui button" id="submitBtn">Submit</button>
                    {buttonClicked
                        ? <OutputForm jsonObject={this.state.jsonData} />
                        : <></>
                    }
                </form>
            </div>
        )
    }
}
