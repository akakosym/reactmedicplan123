import React from 'react'
import 'semantic-ui-css/semantic.min.css'
import { Container } from 'semantic-ui-react'
import InputArea from './components/InputArea.js'

const App = () => {
  return (
    <Container>
      <InputArea />
    </Container>
  )
}

export default App
